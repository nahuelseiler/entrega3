package ar.edu.unc.famaf.redditreader.ui;

import android.content.Context;
import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import java.util.Date;
import java.util.List;

import ar.edu.unc.famaf.redditreader.R;
import ar.edu.unc.famaf.redditreader.model.PostModel;

/**
 * Created by nahuelseiler on 10/10/16.
 */

public class PostAdapter extends ArrayAdapter<PostModel> {

    private List<PostModel> postLst;
    int resourceID;

    public PostAdapter(Context context, int textViewResourceId, List<PostModel> postlist) {
        super(context, textViewResourceId);
        postLst = postlist;
        resourceID = textViewResourceId;
    }

    static class ViewHolder {
        TextView titulo;
        TextView autor;
        TextView date;
        TextView num;
        ImageView imagePost;
    }

    @Override
    public int getCount() {
        return postLst.size();
    }

    @Override
    public int getPosition(PostModel item) {
        return postLst.indexOf(item);
    }

    @Override
    public PostModel getItem(int position) {
        return postLst.get(position);
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {

        ViewHolder holder = new ViewHolder();

        if (convertView == null) {
            /*LayoutInflater vi = context.getLayoutInflater();*/
            LayoutInflater vi = (LayoutInflater) getContext().getSystemService(Context.LAYOUT_INFLATER_SERVICE);
            convertView = vi.inflate(resourceID, parent, false);

            PostModel pm = postLst.get(position);

            holder.titulo = (TextView) convertView.findViewById(R.id.titlePost);
            holder.autor = (TextView) convertView.findViewById(R.id.autor);
            holder.date = (TextView) convertView.findViewById(R.id.date);
            holder.num = (TextView) convertView.findViewById(R.id.num);
            holder.imagePost = (ImageView) convertView.findViewById(R.id.imagePost);
            convertView.setTag(holder);
        }
        else {
            holder = (ViewHolder) convertView.getTag();
        }

        PostModel pm = postLst.get(position);

        TextView postText = (TextView) convertView.findViewById(R.id.titlePost);
        TextView postAutor = (TextView) convertView.findViewById(R.id.autor);
        TextView postDate = (TextView) convertView.findViewById(R.id.date);
        TextView postNum = (TextView) convertView.findViewById(R.id.num);
        ImageView postImage = (ImageView) convertView.findViewById(R.id.imagePost);

        holder.titulo.setText(pm.getTitulo());
        holder.autor.setText(pm.getAutor());

            /*Date auxfecha = pm.getFechaCreacion();
            String fecha = auxfecha.toString();*/

        holder.date.setText(pm.getFechaCreacion());
        holder.num.setText(pm.getNumComment());
        holder.imagePost.setImageResource(pm.getImagenPostId());

        return convertView;
    }

    @Override
    public boolean isEmpty() {
        return postLst.isEmpty();
    }


}
