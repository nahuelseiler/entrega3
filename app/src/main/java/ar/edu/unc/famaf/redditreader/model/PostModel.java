package ar.edu.unc.famaf.redditreader.model;

import java.util.Date;

public class PostModel {

    private String titulo;
    private String autor;
    private String date;
    private String num;
    private int imagePost;

    public String getTitulo() {

        return titulo;
    }

    public void setTitulo(String titulo) {

        this.titulo = titulo;
    }

    public String getAutor() {

        return autor;
    }

    public void setAutor(String autor) {

        this.autor = autor;
    }

    public String getFechaCreacion() {

        return date;
    }

    public void setFechaCreacion(String fechaCreacion) {

        this.date = fechaCreacion;
    }

    public String getNumComment() {

        return num;
    }

    public void setNumComment(String numComment) {

        this.num = numComment;
    }

    public int getImagenPostId() {

        return imagePost;
    }

    public void setImagenPostId(int imagenPostId) {

        this.imagePost = imagenPostId;
    }

}




