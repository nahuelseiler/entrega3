package ar.edu.unc.famaf.redditreader.backend;

import android.content.Context;
import android.widget.ArrayAdapter;
import android.widget.ImageView;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import ar.edu.unc.famaf.redditreader.R;
import ar.edu.unc.famaf.redditreader.model.PostModel;

public class Backend {

    private static Backend ourInstance = new Backend();

    public static Backend getInstance() {
        return ourInstance;
    }

    private List<PostModel> postLst;

    private Backend() {

        postLst = new ArrayList<>();

        PostModel post0 = new PostModel();
        post0.setTitulo("Especie en Extinción: Caballo Mongol");
        post0.setAutor("John Smith");
        /*Date date = new Date();*/
        post0.setFechaCreacion("Ayer");
        post0.setNumComment("147");
        post0.setImagenPostId(R.mipmap.caballo);

        PostModel post1 = new PostModel();
        post1.setTitulo("SETI: Search for ExtraTerrestrial Intelligence");
        post1.setAutor("Math Daniels");
        /*Date date1 = new Date();*/
        post1.setFechaCreacion("Hace 24 hs");
        post1.setNumComment("250");
        post1.setImagenPostId(R.mipmap.seti);

        PostModel post2 = new PostModel();
        post2.setTitulo("Redes Neuronales en Cs. Computación");
        post2.setAutor("Juan Dominguez");
        /*Date date2 = new Date();*/
        post2.setFechaCreacion("Hace 3 hs");
        post2.setNumComment("150");
        post2.setImagenPostId(R.mipmap.neural);

        PostModel post3 = new PostModel();
        post3.setTitulo("Nuevo caza furtivo ruso: PAKFA T50");
        post3.setAutor("Vadim Vochsnik");
        /*Date date3 = new Date();*/
        post3.setFechaCreacion("Hace 30 min.");
        post3.setNumComment("200");
        post3.setImagenPostId(R.mipmap.pakfa);

        PostModel post4 = new PostModel();
        post4.setTitulo("Plantas carnivoras: la nueva moda en los jardines");
        post4.setAutor("Magdalena De la Puente");
        /*Date date4 = new Date();*/
        post4.setFechaCreacion("Ahora");
        post4.setNumComment("100");
        post4.setImagenPostId(R.mipmap.carnivora);

        postLst.add(post0);
        postLst.add(post1);
        postLst.add(post2);
        postLst.add(post3);
        postLst.add(post4);
    }

    public List<PostModel> getTopPosts() {
        return postLst;
    }
}